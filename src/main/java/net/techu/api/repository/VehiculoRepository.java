package net.techu.api.repository;

import net.techu.api.models.Vehiculo;

import java.util.List;

public interface VehiculoRepository {

    List<Vehiculo> findAll();
    Vehiculo findOne(String id);
    public Vehiculo saveVehiculo(Vehiculo vehiculo);
    public void updateVehiculo(Vehiculo vehiculo);
    public void deleteVehiculo(String id);
}
