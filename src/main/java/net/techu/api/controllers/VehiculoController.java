package net.techu.api.controllers;

import net.techu.api.models.Vehiculo;
import net.techu.api.service.VehiculoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("vehiculos")
public class VehiculoController {

    private final VehiculoService vehiculoService;
    private Vehiculo vehiculo;

    @Autowired
    public VehiculoController(VehiculoService vehiculoService) {
        this.vehiculoService = vehiculoService;
    }

    @GetMapping()
    public ResponseEntity<List<Vehiculo>> vehiculos(){
        System.out.println("Me piden la lista de vehiculos");
        return ResponseEntity.ok(vehiculoService.findAll());
    }

    @PostMapping
    public ResponseEntity<Vehiculo> saveVehiculo(@RequestBody Vehiculo vehiculo){
        return ResponseEntity.ok(vehiculoService.saveVehiculo(vehiculo));
    }
}
