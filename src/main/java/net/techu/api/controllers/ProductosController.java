package net.techu.api.controllers;

import net.techu.api.Producto;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@RestController
public class ProductosController {

    private static Map<String, Producto> listaProductos = new HashMap<>();

    static {
        Producto pr1 = new Producto();
        pr1.setId("PR1");
        pr1.setNombre("PRODUCTO 1");

        Producto pr2 = new Producto();
        pr2.setId("PR2");
        pr2.setNombre("PRODUCTO 2");

        listaProductos.put("PR1", pr1);
        listaProductos.put("PR2", pr2);
    }

    @GetMapping("/productos")
    public Object getProductos(){
        return listaProductos;
    }

    @Autowired
    ResourceLoader cargador;

    @GetMapping("/v2/productos")
    public String getProductosDeFicheros(){
        String nombreFichero = "classpath:static/products.txt";
        Resource recursoFichero = cargador.getResource(nombreFichero);
        String productos = "";
        try {
            InputStream streamDatos = recursoFichero.getInputStream();
            byte[] bytesDocumento = streamDatos.readAllBytes();
            productos = new String(bytesDocumento, StandardCharsets.UTF_8);
            streamDatos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return productos;
    }

    @GetMapping("/v3/productos")
    public String getProductosDeOtraAPI(){
        String urlAPI = "https://services.odata.org/V4/OData/OData.svc/Products";
        String productos = "";
        try {
            URL url = new URL(urlAPI);
            HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setDoInput(true);
            conexion.setDoOutput(true);
            //DataOutputStream salida = new DataOutputStream(conexion.getOutputStream());
            //DataInputStream entrada = new DataInputStream(conexion.getInputStream());
            InputStreamReader entrada = new InputStreamReader(conexion.getInputStream());
            int respuesta = conexion.getResponseCode();
            System.out.println("Respuesta: " + String.valueOf(respuesta));
            if(respuesta == HttpURLConnection.HTTP_OK){
                BufferedReader lector = new BufferedReader(entrada);
                String lineaLeida;
                StringBuffer resultado = new StringBuffer();
                while((lineaLeida = lector.readLine()) != null){
                    resultado.append(lineaLeida);
                }
                lector.close();
                productos = resultado.toString();
            }
        }catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return productos;
    }

    @PostMapping("/v2/productos/{formato}")
    public ResponseEntity<Object> addProductoFichero(@RequestBody Producto producto, @PathVariable("formato") String formato){
        String nombreFichero;
        Resource recursoFichero;
        if(formato.toUpperCase().equals("JSON")){
            nombreFichero = "classpath:static/productos.json";
            recursoFichero = cargador.getResource(nombreFichero);
            JSONObject nuevo = new JSONObject();
            nuevo.put("id", producto.getId());
            nuevo.put("nombre", producto.getNombre());
            File fichero = null;
            try {
                fichero = recursoFichero.getFile();
                OutputStreamWriter escritor = new FileWriter(fichero, true);
                escritor.append(nuevo.toString());
                escritor.close();
                return new ResponseEntity<>("Producto JSON añadido al fichero", HttpStatus.CREATED);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return new ResponseEntity<>("Error al añadir el fichero", HttpStatus.BAD_REQUEST);
        } else {
            nombreFichero = "classpath:static/products.txt";
            recursoFichero = cargador.getResource(nombreFichero);
            try {
                File fichero = recursoFichero.getFile();
                OutputStreamWriter escritor = new FileWriter(fichero, true);
                escritor.append(String.format("Nuevo producto: %s %s", producto.getId(), producto.getNombre()));
                escritor.flush();
                escritor.close();
                return new ResponseEntity<>("Producto añadido al fichero", HttpStatus.CREATED);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return new ResponseEntity<>("Error al añadir el fichero", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/productos/{id}")
    public ResponseEntity<Object> getProducto(@PathVariable("id") String id){
        if(listaProductos.containsKey(id.toUpperCase())){
            return new ResponseEntity<>(listaProductos.get(id.toUpperCase()), HttpStatus.OK);
        }
        return new ResponseEntity<>(String.format("Producto no encontrado %s", id), HttpStatus.NOT_FOUND);
    }

    @PostMapping("/productos")
    public ResponseEntity<Object> addProducto(@RequestBody Producto producto){
        if(listaProductos.containsKey(producto.getId())){
            return new ResponseEntity<>("Producto ya existente", HttpStatus.BAD_REQUEST);
        }
        listaProductos.put(producto.getId(), producto);
        return new ResponseEntity<>(String.format("Producto %s dado de alta", producto.getId()), HttpStatus.CREATED);
    }

    @PutMapping("/productos/{id}")
    public ResponseEntity<Object> updateProducto(@PathVariable("id") String id, @RequestBody Producto producto){
        if(!id.toUpperCase().equals(producto.getId().toUpperCase())){
            return new ResponseEntity<>("Datos incorrectos", HttpStatus.BAD_REQUEST);
        }
        if(listaProductos.containsKey(id.toUpperCase())){
            listaProductos.put(id, producto);
            return new ResponseEntity<>(String.format("Producto %s actualizado", id), HttpStatus.CREATED);
        }
        listaProductos.put(id, producto);
        return new ResponseEntity<>(String.format("Producto %s creado", id), HttpStatus.CREATED);
    }

    @DeleteMapping("/productos/{id}")
    public ResponseEntity<Object> deleteProducto(@PathVariable("id") String id){
        if(listaProductos.containsKey(id.toUpperCase())){
            listaProductos.remove(id.toUpperCase());
            return new ResponseEntity<>(String.format("Producto %s es eliminado", id), HttpStatus.CREATED);
        }
        return new ResponseEntity<>(String.format("Producto %s no encontrado", id), HttpStatus.NOT_FOUND);
    }

    //@RequestMapping("/productos")
    public String productos(){
        return "Lista de productos";
    }

    //@RequestMapping("/productos")
    public String productos(@RequestParam(value = "id", required = true) String id){
        return String.format("Info del producto %s", id);
    }
}
