package net.techu.api.controllers;

import net.techu.api.Saludo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HolaController {

    //Se usa por convenio
    @RequestMapping("/")
    public Object index(){
        Saludo saludo = new Saludo("Hola desde Spring Boot");
        return saludo;
    }
}
