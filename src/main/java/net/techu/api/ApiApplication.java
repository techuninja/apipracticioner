package net.techu.api;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

//@SpringBootApplication
public class ApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunnerLog(ApplicationContext ctx){
		return args -> {
			System.out.println("Spring Boot funcionando");
		};
	}

	@Bean
	public CommandLineRunner commandLineRunnerEstado(ApplicationContext ctx){
		return args -> {
			Integer totalBeans = ctx.getBeanDefinitionCount();
			String[] beans = ctx.getBeanDefinitionNames();
			Arrays.sort(beans);
			System.out.println("TOTAL BEANS");
			System.out.println(totalBeans);
			for (String bean : beans) {
				System.out.println(bean);
			}
		};
	}
}
