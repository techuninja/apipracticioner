package net.techu.api.service;

import net.techu.api.models.Vehiculo;
import net.techu.api.repository.VehiculoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("vehiculoService")
@Transactional
public class VehiculoServiceImpl implements VehiculoService{

    private VehiculoRepository vehiculoRepository;

    @Autowired
    public VehiculoServiceImpl(VehiculoRepository vehiculoRepository) {
        this.vehiculoRepository = vehiculoRepository;
    }

    @Override
    public List<Vehiculo> findAll() {
        return vehiculoRepository.findAll();
    }

    @Override
    public Vehiculo findOne(String id) {
        return vehiculoRepository.findOne(id);
    }

    @Override
    public Vehiculo saveVehiculo(Vehiculo vehiculo) {
        return vehiculoRepository.saveVehiculo(vehiculo);
    }

    @Override
    public void updateVehiculo(Vehiculo vehiculo) {
        vehiculoRepository.updateVehiculo(vehiculo);
    }

    @Override
    public void deleteVehiculo(String id) {
        vehiculoRepository.deleteVehiculo(id);
    }
}
