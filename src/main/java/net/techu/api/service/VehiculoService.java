package net.techu.api.service;

import net.techu.api.models.Vehiculo;

import java.util.List;

public interface VehiculoService {

    List<Vehiculo> findAll();
    Vehiculo findOne(String id);
    public Vehiculo saveVehiculo(Vehiculo vehiculo);
    public void updateVehiculo(Vehiculo vehiculo);
    public void deleteVehiculo(String id);
}
