package net.techu.api;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class HolaControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void getSaludo() throws Exception{
        mvc.perform(MockMvcRequestBuilders.get("/"))
            .andExpect(status().isOk())
                .andExpect(content().json("{ \"data\": \"Hola desde Spring Boot\" }"));
                //.andExpect(content().string(equalTo("Hola desde Spring Boot")));
    }
}