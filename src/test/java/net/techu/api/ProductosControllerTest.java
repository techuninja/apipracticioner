package net.techu.api;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ProductosControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void getProductoOK() throws Exception{
        mvc.perform(MockMvcRequestBuilders.get("/productos"))
                .andExpect(status().isOk());
    }

    @Test
    public void getProductoInfo() throws Exception{
        mvc.perform(MockMvcRequestBuilders.get("/productos"))
                .andExpect(status().isOk());
    }

    @Test
    public void getProductoExistente() throws Exception{
        mvc.perform(MockMvcRequestBuilders.get("/productos/PR1"))
                .andExpect(status().isOk());
    }

    @Test
    public void getProductoInexistente() throws Exception{
        mvc.perform(MockMvcRequestBuilders.get("/productos/XXX"))
                .andExpect(status().isNotFound());
    }
}
